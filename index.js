'use strict';

const frontmatter = require('remark-frontmatter');
const parse_markdown = require('remark-parse');
const parse_yaml = require('remark-parse-yaml');
const math = require('remark-math');
const unified = require('unified');

const stringify = require('./stringify.js');

module.exports = unified()
    .use(parse_markdown)
    .use(frontmatter)
    .use(parse_yaml)
    .use(stringify)
    .use(math)
    .freeze();

