'use strict';

const crypto = require('crypto');
const fs = require('fs');
const json5 = require('json5');
const mime = require('mime');
const path = require('path');
const process = require('child_process');
const stringify = require('remark-stringify');
const unherit = require('unherit');
const visit = require('unist-util-visit');
const xtend = require('xtend');

module.exports = repynb_stringify;

function repynb_stringify(opts) {
    const options = xtend(this.data('settings'), opts);
    this.Compiler = unherit(stringify.Compiler);
    this.Compiler.prototype.setOptions({
        fences: true,
        fence: '~',
        rule: '-',
        strong: '*',
        emphasis: '*'
    })
    this.Compiler.prototype.visitors.root = function(node) {
        const self = this;

        if (node.children[0].type === 'yaml') {
            const yaml = node.children.shift();
            var metadata = xtend(
                options.defaultMetadata,
                (yaml.data || {}).parsedValue);
        } else {
            var metadata = xtend(options.defaultMetadata);
        }

        if (!metadata.title) {
            const first = node.children[0];
            if (first.type === 'heading' && first.depth === 1) {
                metadata.title = self.block({
                    type: 'paragraph',
                    children: first.children
                });
            }
        }

        const execute = metadata.execute;
        delete metadata.execute;

        const cells = [];
        let cur = [];

        function finish_cur() {
            if (cur.length === 0) return;

            const tree = {
                type: 'root',
                children: cur,
            };
            cur = [];

            const attachments = {}
            if (options.attachImages) {
                visit(tree, 'image', function(node) {
                    if (!node.url || node.url.includes('://')) return;

                    const mimetype = mime.getType(path.extname(node.url));
                    if (!mimetype) return;

                    const attachment = {};
                    const data = fs.readFileSync(node.url, 'base64');
                    attachment[mimetype] = data;

                    const name = crypto.createHash('sha256').update(data).digest('base64');
                    attachments[name] = attachment;

                    node.url = `attachment:${name}`;
                });
            }

            cells.push({
                cell_type: 'markdown',
                metadata: {},
                source: self.block(tree),
                attachments: attachments,
            });
        }

        node.children.forEach(function(child) {
            if (child.type === 'code' && child.meta) {
                let [first, ...rest] = child.meta.split(' ');
                if (first === 'cell') {
                    finish_cur();
                    let nrest = rest.length;
                    rest = rest.filter(function(elm) { return elm !== 'raises'; });
                    let raises = (rest.length !== nrest);
                    let cellmeta = json5.parse('{' + rest.join(' ') + '}');
                    if (raises) {
                        cellmeta.tags = cellmeta.tags || [];
                        cellmeta.tags.push('raises-exception');
                    }
                    cells.push({
                        cell_type: 'code',
                        metadata: cellmeta,
                        source: child.value,
                        execution_count: null,
                        outputs: [],
                    });
                    return;
                }
            }
            cur.push(child);
        });

        finish_cur();

        const notebook = JSON.stringify({
            nbformat: 4,
            nbformat_minor: 2,
            metadata: metadata,
            cells: cells,
        });

        if (options.allowExecute && execute) {
            const args = [ '--to', 'notebook', '--stdin', '--stdout', '-y', '--execute' ];
            if (Array.isArray(execute)) {
                args.push(...execute);
            }
            return process.execFileSync('jupyter-nbconvert', args, { input: notebook });
        } else {
            return notebook;
        }
    }
}
